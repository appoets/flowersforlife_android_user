package com.copiaexigo.grocery.users.models.specifiedshop;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CategoriesItem implements Serializable {

	@SerializedName("latitude")
	private Double latitude;

	@SerializedName("rating")
	private Integer rating;

	@SerializedName("description")
	private String description;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("pure_veg")
	private Integer pureVeg;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("ratings")
	private Object ratings;

	@SerializedName("estimated_delivery_time")
	private Integer estimatedDeliveryTime;

	@SerializedName("timings")
	private List<TimingsItem> timings;

	@SerializedName("currency")
	private String currency;

	@SerializedName("id")
	private Integer id;

	@SerializedName("default_banner")
	private String defaultBanner;

	@SerializedName("categories")
	private List<CategoriesItem> categories;

	@SerializedName("maps_address")
	private String mapsAddress;

	@SerializedName("popular")
	private Integer popular;

	@SerializedName("email")
	private String email;

	@SerializedName("offer_min_amount")
	private Integer offerMinAmount;

	@SerializedName("longitude")
	private Double longitude;

	@SerializedName("offer_percent")
	private Integer offerPercent;

	@SerializedName("address")
	private String address;

	@SerializedName("device_id")
	private Object deviceId;

	@SerializedName("otp")
	private Object otp;

	@SerializedName("avatar")
	private String avatar;

	@SerializedName("deleted_at")
	private Object deletedAt;

	@SerializedName("cuisines")
	private List<Object> cuisines;

	@SerializedName("rating_status")
	private Integer ratingStatus;

	@SerializedName("phone")
	private String phone;

	@SerializedName("device_token")
	private Object deviceToken;

	@SerializedName("name")
	private String name;

	@SerializedName("favorite")
	private Object favorite;

	@SerializedName("status")
	private String status;

	@SerializedName("shop_id")
	private Integer shopId;

	@SerializedName("parent_id")
	private Integer parentId;

	@SerializedName("position")
	private Integer position;

	@SerializedName("products")
	private List<ProductsItem> products;

	public void setLatitude(Double latitude){
		this.latitude = latitude;
	}

	public Double getLatitude(){
		return latitude;
	}

	public void setRating(Integer rating){
		this.rating = rating;
	}

	public Integer getRating(){
		return rating;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDeviceType(String deviceType){
		this.deviceType = deviceType;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public void setPureVeg(Integer pureVeg){
		this.pureVeg = pureVeg;
	}

	public Integer getPureVeg(){
		return pureVeg;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setRatings(Object ratings){
		this.ratings = ratings;
	}

	public Object getRatings(){
		return ratings;
	}

	public void setEstimatedDeliveryTime(Integer estimatedDeliveryTime){
		this.estimatedDeliveryTime = estimatedDeliveryTime;
	}

	public Integer getEstimatedDeliveryTime(){
		return estimatedDeliveryTime;
	}

	public void setTimings(List<TimingsItem> timings){
		this.timings = timings;
	}

	public List<TimingsItem> getTimings(){
		return timings;
	}

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		return currency;
	}

	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return id;
	}

	public void setDefaultBanner(String defaultBanner){
		this.defaultBanner = defaultBanner;
	}

	public String getDefaultBanner(){
		return defaultBanner;
	}

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}

	public void setMapsAddress(String mapsAddress){
		this.mapsAddress = mapsAddress;
	}

	public String getMapsAddress(){
		return mapsAddress;
	}

	public void setPopular(Integer popular){
		this.popular = popular;
	}

	public Integer getPopular(){
		return popular;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setOfferMinAmount(Integer offerMinAmount){
		this.offerMinAmount = offerMinAmount;
	}

	public Integer getOfferMinAmount(){
		return offerMinAmount;
	}

	public void setLongitude(Double longitude){
		this.longitude = longitude;
	}

	public Double getLongitude(){
		return longitude;
	}

	public void setOfferPercent(Integer offerPercent){
		this.offerPercent = offerPercent;
	}

	public Integer getOfferPercent(){
		return offerPercent;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setDeviceId(Object deviceId){
		this.deviceId = deviceId;
	}

	public Object getDeviceId(){
		return deviceId;
	}

	public void setOtp(Object otp){
		this.otp = otp;
	}

	public Object getOtp(){
		return otp;
	}

	public void setAvatar(String avatar){
		this.avatar = avatar;
	}

	public String getAvatar(){
		return avatar;
	}

	public void setDeletedAt(Object deletedAt){
		this.deletedAt = deletedAt;
	}

	public Object getDeletedAt(){
		return deletedAt;
	}

	public void setCuisines(List<Object> cuisines){
		this.cuisines = cuisines;
	}

	public List<Object> getCuisines(){
		return cuisines;
	}

	public void setRatingStatus(Integer ratingStatus){
		this.ratingStatus = ratingStatus;
	}

	public Integer getRatingStatus(){
		return ratingStatus;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDeviceToken(Object deviceToken){
		this.deviceToken = deviceToken;
	}

	public Object getDeviceToken(){
		return deviceToken;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setFavorite(Object favorite){
		this.favorite = favorite;
	}

	public Object getFavorite(){
		return favorite;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setShopId(Integer shopId){
		this.shopId = shopId;
	}

	public Integer getShopId(){
		return shopId;
	}

	public void setParentId(Integer parentId){
		this.parentId = parentId;
	}

	public Integer getParentId(){
		return parentId;
	}

	public void setPosition(Integer position){
		this.position = position;
	}

	public Integer getPosition(){
		return position;
	}

	public void setProducts(List<ProductsItem> products){
		this.products = products;
	}

	public List<ProductsItem> getProducts(){
		return products;
	}
}