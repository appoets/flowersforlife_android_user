package com.copiaexigo.grocery.users.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class HomeCategoryResponse {

	@SerializedName("categories")
	private List<CategoriesItem> categories;

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}
}